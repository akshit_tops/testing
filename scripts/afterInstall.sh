#!/bin/bash
# Check if env file exists
INITDIR=`pwd`
cd /var/www/html/pipe/

export COMPOSER_HOME=/home/ec2-user

# Start composer install
composer install
composer dumpautoload

FILE=.env
if ! test -f "$FILE"; then
	echo ".env not found, setting up the .env"
    cp prod.env .env
    # cd ../dev
    php artisan cache:clear
    php artisan config:clear
    # cd ../scripts
fi

php artisan migrate

cd "$INITDIR"