#!/bin/bash
### This script will initialize the Laravel installation. ###
echo "Checking for unmet dependencies"

# Check if php is installed and fulfills the minimum laravel requirements
# Check if php is installed
phploc=`which php`

if [ -z "$phploc" ]
then
	echo "PHP not installed, exiting" >&2
	exit 1
fi

# Check php version
phpver=`php -v |grep -Eow '^PHP [^ ]+' |gawk '{ print $2 }'`
minver=7.2.5
verlte() {
    [  "$1" = "`echo -e "$1\n$2" | sort -V | head -n1`" ]
}

if verlte $phpver $minver
then
	# php version is smaller
	echo "Current PHP version $phpver is lesser than required $minver, exiting" >&2
	exit 1
fi

# Check for all the required PHP extensions, install them if required
extension_issue=false
allextension=(bcmath ctype fileinfo json mbstring openssl pdo_mysql tokenizer xml)

for ext in ${allextension[@]}; do
	phpmod=`php -m | grep "$ext"`
	if [ -z "$phpmod" ]
	then
		extension_issue=true
		echo "PHP extension $ext is not installed" >&2
	fi
done

if $extension_issue; then
	echo "Please install the extensions before continuing" >&2
	exit 1
fi

# Check if composer is installed, install if required
composerloc=`which composer`

if [ -z "$composerloc" ]
then
	echo "Composer is not installed, exiting" >&2
	exit 1
fi
# Check if mysql is installed and running
# mysqlloc=`which mysql`

# if [ -z "$mysqlloc" ]
# then
# 	echo "MySQL is not installed, exiting" >&2
# 	exit 1
# fi

# # Check if mysql user exists
# user=akshit
# password=123456
# database=mysql
# RESULT=`mysql -u$user -p$password -e "SHOW DATABASES" | grep $database`
# if [ "$RESULT" == "$database" ]; then
# 	echo "Database exist"
# else
# 	echo "Database or user does not exist" >&2
# 	exit 1
# fi